## Encontrar o IP

    ## TEXTO
    126.1.112.34
    128.126.12.244
    192.168.0.34

    ## REGEX
    \d{0,n} => Pode ter ZERO ou N numeros na sequencia
    \d{0, 9}=> "123456789"

    \d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}