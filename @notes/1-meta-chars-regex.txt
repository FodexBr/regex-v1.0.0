# Caracteres da Regex: 

    ## Meta-Chars OU Meta-caractres ##
    Meta Chars tem um significado para a RegexEngine
    ou seja esses caractres tem um valor semântico para a Engine da Regex

    .  (ponto final) => qualquer Caracteres
    *  (asteriscco) => zero, um ou mais vezes
    +  (operador soma) => um ou mais vezes
    \s (barra s) => whitespaces(spacos em brancos)
    \s{1,} => \s com qauntifier pode ser (1 espacos ou n vezes)
    \s+ => \s com meta-char + : pode ser (1 espcos ou mais vezes)
    ?  (Interrogacao) => Zero ou Uma Vez(Caractere Opcional)
    () => parenteses tambem são meta-chars
    {} (chaves) => definem quantidade de Caracteres ,
    as {} tem um outro valor semantico por serem META-CHARS são 
    chamados de "QUANTIFIERS", pois define uma quantidade de caractres
    a serem encontrados


        ## Exemplos de Uso De Meta-Chars:
        ex: \d* => pega zero, um, ou mais digitos
        ex: .*png => pega quanquer digito(.), e pega zero, um ou mais vezes "png"(*png)

        ## Exemplos de Uso de Metac-Chars QUANTIFIERS
        ex: a{3} => seleciona uma string que cotem 3 a: "aaa";
        ex: \d{3} => pega 3 digitos 

        ## Exemplos de Uso de Meta-Chars "Caracteres Opcionais" ?
        Nesse exeplo "Podemos ter Zero ou Uma Vez o '.'"
        \d{3}\.?\d{3}\.?\d{3}

        O meta-char ? é um atalho para
        \d{3}\.{0,1}

    ## Scape de metachars:
    Para scapar um metachar prescisamos colocar o '\'
    ex: \. => o ponto literar
    ex: . =>  meta-char



## Mais detalhes: META-CHARS
. - Qualquer Caractere
? - Zero ou Uma vez
* - Zero, Um, ou mais Vezes
+ - Um ou mais vezes
{n} - Exatamente N vezes
{n,} - N ou mais Vezes
{n,m} - no minimo  N vezes , no maximo M vezes 
\d pega digitos de 0 A 9 equivale a uma classe de caractres [0-9]
\D pega letras de A-Za-z equivale a uma classe de caractres [A-Za-z]
\w equivale a QUALQUER CARECTERE, incluindo letras e numermos, é um atalho para [A-Za-z0-9_]
\w+ pega QUALQUER Caractere UMA OU MAIS VEZES
+? => Carregamento Preguiçosos(Leazy)
^ => deve inciiar com
$ => deve finalizar com 
[^caractres] negacao, pega tudo que nao vinher a direita do operador ^[caractres]

## Mais Detalhes: Classes de Caractres
[A-Z]
[a-z]
[0-9]
[0-9A-Za-z_] OU \w


## Mais detalhes: Grupos
() => define um grupo
(?:) => Define para nao indentificra o grupo
